import React from "react";
import { Text, View } from "react-native";

const ItemPage = ({route}) => {
  const item = route.params.selectedItem;
  return (
    <View>
      <Text>{item.id}</Text>
      <Text>{item.name}</Text>
      <Text>{item.color}</Text>
    </View>
  );
};

export default ItemPage;
