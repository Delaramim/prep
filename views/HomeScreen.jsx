import React from "react";
import { View, Text, Button } from "react-native";
import { addItem, removeItem, addToCart, clearAll } from "../nameSlice.js";
import { useDispatch, useSelector } from "react-redux";

const Home = ({ navigation }) => {
  const dispatch = useDispatch();

  const { items } = useSelector((state) => state.items);
  const { cart } = useSelector((state) => state.cart);
  return (
    <View>
      <Text>Home page</Text>
      {items.map((item) => {
        return (
          <View style={{ padding: 20 }}>
            <Text>{item.name}</Text>
            <Text style={{ color: item.color }}>{item.color}</Text>
            <Button
              title="Add to Cart"
              onPress={() => dispatch(addToCart({ id: item.id }))}
            />
            <Button
              title="Open details"
              onPress={() =>
                navigation.navigate("Item", { selectedItem: item })
              }
            />
          </View>
        );
      })}
      <Button
        title="add"
        onPress={() =>
          dispatch(addItem({ id: 4, name: "test", color: "pink" }))
        }
      ></Button>
      <Button
        title="remove"
        onPress={() => dispatch(removeItem({ id: 1 }))}
      ></Button>
      <Button title="Clear Cart" onPress={() => dispatch(clearAll())} />
      <Button title="consoleitems" onPress={() => console.log(items)} />
      <Button title="consolecart" onPress={() => console.log(cart)} />
    </View>
  );
};

export default Home;
