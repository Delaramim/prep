import { createSlice, configureStore } from "@reduxjs/toolkit";

const namesSlice = createSlice({
  name: "items",
  initialState: {
    items: [
      { id: 1, name: "apple", color: "red" },
      { id: 2, name: "banana", color: "yellow" },
      { id: 3, name: "watermelon", color: "green" },
    ],
    cart: []
  },
  reducers: {
    addItem: (state, action) => {
      return { ...state, items: [...state.items, action.payload] };
    },
    removeItem: (state, action) => {
      return {
        items: [...state.items.filter((item) => item.id !== action.payload.id)],
      };
    },
    addToCart: (state, action) => {
      return {
        ...state,
        cart: [...state.cart, ...state.items.filter((item) => item.id === action.payload.id)]
      }
    },
    removeFromCart: (state, action) => {
      return {
        cart: [...state.cart.filter((item) => item.id !== action.payload.id)],
      }
    },
    clearAll: (state) => {
      return {
        ...state,
        cart: []
      }
    }
  },
});

const { addItem, removeItem, addToCart, clearAll } = namesSlice.actions;

export const store = configureStore({
  reducer: {
    items: namesSlice.reducer,
    cart: namesSlice.reducer
  },
});

export { addItem, removeItem, addToCart, clearAll };
